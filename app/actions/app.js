import * as TYPES from 'app-types';

export const fetchRepos = () => ({
  type: TYPES.FETCH_REPOS,
});

export const fetchDetails = () => ({
  type: TYPES.FETCH_REPO_DETAILS,
});

export const fetchContributors = () => ({
  type: TYPES.FETCH_REPO_CONTRIBUTORS,
});

export const selectRepo = repo => ({
  type: TYPES.SELECT_REPO,
  repo,
});

export const updateLoadingStatus = loading => ({
  type: TYPES.UPDATE_LOADING_STATUS,
  loading,
});

export const receiveRepos = data => ({
  type: TYPES.FETCH_REPOS_SUCCESS,
  data,
});

export const receiveDetails = data => ({
  type: TYPES.FETCH_REPO_DETAILS_SUCCESS,
  data,
});

export const receiveContributors = data => ({
  type: TYPES.FETCH_REPO_CONTRIBUTORS_SUCCESS,
  data,
});
