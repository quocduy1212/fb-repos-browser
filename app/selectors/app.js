import _ from 'lodash';
import { createSelector } from 'reselect';

const stateApp = state => state.app || {};
const stateRepos = createSelector(stateApp, app => app.repos || {});
const stateDetails = createSelector(stateApp, app => app.details || {});
const stateContributors = createSelector(
  stateApp,
  app => app.contributors || {}
);

export const getLoading = createSelector(stateApp, app => app.loading);
export const getSelectedRepo = createSelector(stateApp, app => app.selected);
export const getRepos = createSelector(stateRepos, repos =>
  _.orderBy(repos.data || [], 'watchers_count', 'desc')
);
export const getDefaultRepo = createSelector(
  getRepos,
  repos => (repos.length ? repos[0] : null)
);
export const getDetails = createSelector(
  stateDetails,
  details => details.data || {}
);
export const getContributors = createSelector(
  stateContributors,
  contributors => contributors.data || []
);

export const getReposError = createSelector(
  stateRepos,
  repos => repos.error || ''
);
export const getDetailsError = createSelector(
  stateDetails,
  details => details.error || ''
);
export const getContributorsError = createSelector(
  stateContributors,
  contributors => contributors.error || ''
);
