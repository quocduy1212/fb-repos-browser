import * as TYPES from 'app-types';

const DEFAULT_STATE = {
  loading: {
    fetchRepos: false,
    fetchRepoDetails: true,
    fetchRepoContributors: true,
  },
};

const handlers = {
  [TYPES.FETCH_REPOS_SUCCESS]: (state, action) => ({
    ...state,
    repos: action.data,
  }),
  [TYPES.FETCH_REPOS_FAIL]: (state, action) => ({
    ...state,
    repos: action.error,
  }),
  [TYPES.FETCH_REPO_DETAILS_SUCCESS]: (state, action) => ({
    ...state,
    details: action.data,
  }),
  [TYPES.FETCH_REPO_DETAILS_FAIL]: (state, action) => ({
    ...state,
    details: action.error,
  }),
  [TYPES.FETCH_REPO_CONTRIBUTORS_SUCCESS]: (state, action) => ({
    ...state,
    contributors: action.data,
  }),
  [TYPES.FETCH_REPO_CONTRIBUTORS_FAIL]: (state, action) => ({
    ...state,
    contributors: action.error,
  }),

  [TYPES.UPDATE_LOADING_STATUS]: (state, action) => ({
    ...state,
    loading: {
      ...state.loading,
      ...action.loading,
    },
  }),
  [TYPES.SELECT_REPO]: (state, action) => ({
    ...state,
    selected: action.repo,
  }),
};

const app = (state = DEFAULT_STATE, action) =>
  handlers[action.type] ? handlers[action.type](state, action) : state;

export default app;
