import { takeLatest, call, put, select } from 'redux-saga/effects';
import * as appSelectors from 'app-selectors/app';
import * as apis from 'app-libs/api';
import * as TYPES from 'app-types';
import * as appActions from 'app-actions/app';

function* fetchRepos() {
  try {
    yield put(appActions.updateLoadingStatus({ fetchRepos: true }));
    const repos = yield call(apis.fetchRepos);
    yield put(appActions.receiveRepos(repos));
    yield put(appActions.updateLoadingStatus({ fetchRepos: false }));
    const state = yield select();
    const selected = appSelectors.getSelectedRepo(state);
    if (!selected) {
      const repo = appSelectors.getDefaultRepo(state);
      if (repo) {
        yield put(appActions.selectRepo(repo));
      }
    }
  } catch (error) {
    yield put(appActions.updateLoadingStatus({ fetchRepos: false }));
    yield put({ type: TYPES.FETCH_REPOS_FAIL, error });
  }
}
function* fetchRepoDetails() {
  try {
    yield put(appActions.updateLoadingStatus({ fetchRepoDetails: true }));
    const state = yield select();
    const selected = appSelectors.getSelectedRepo(state);
    const details = yield call(apis.fetchRepoDetails, selected.name);
    yield put(appActions.receiveDetails(details));
    yield put(appActions.updateLoadingStatus({ fetchRepoDetails: false }));
  } catch (error) {
    yield put(appActions.updateLoadingStatus({ fetchRepoDetails: false }));
    yield put({ type: TYPES.FETCH_REPO_DETAILS_FAIL, error });
  }
}

function* fetchRepoContributors() {
  try {
    yield put(appActions.updateLoadingStatus({ fetchRepoContributors: true }));
    const state = yield select();
    const selected = appSelectors.getSelectedRepo(state);
    const contributors = yield call(apis.fetchRepoContributors, selected.name);
    yield put(appActions.receiveContributors(contributors));
    yield put(appActions.updateLoadingStatus({ fetchRepoContributors: false }));
  } catch (error) {
    yield put(appActions.updateLoadingStatus({ fetchRepoContributors: false }));
    yield put({ type: TYPES.FETCH_REPO_CONTRIBUTORS_FAIL, error });
  }
}

function* fetchAllDetails() {
  yield put(appActions.fetchDetails());
  yield put(appActions.fetchContributors());
}

function* saga() {
  yield takeLatest(TYPES.FETCH_REPOS, fetchRepos);
  yield takeLatest(TYPES.FETCH_REPO_DETAILS, fetchRepoDetails);
  yield takeLatest(TYPES.FETCH_REPO_CONTRIBUTORS, fetchRepoContributors);
  yield takeLatest(TYPES.SELECT_REPO, fetchAllDetails);
}

export default saga;
