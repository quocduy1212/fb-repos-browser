import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import Home from './components/home';

const Routes = () => (
  <BrowserRouter>
    <div>
      <Route exact path="/:repo?" component={Home} />
    </div>
  </BrowserRouter>
);

export default Routes;
