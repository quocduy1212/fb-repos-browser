import React, { Fragment } from 'react';
import PropTypes from 'prop-types';

const SimpleLoader = () => <div>loading...</div>;

const withLoader = Inner => {
  const WithLoader = ({ isLoading, ...rest }) => (
    <Fragment>
      {isLoading && <SimpleLoader />}
      {!isLoading && <Inner {...rest} />}
    </Fragment>
  );

  WithLoader.propTypes = {
    isLoading: PropTypes.bool.isRequired,
  };

  WithLoader.displayName = `WithLoader(${Inner.displayName || Inner.name})`;

  return WithLoader;
};

export default withLoader;
