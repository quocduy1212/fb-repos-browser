import GitHub from 'github-api';

const gh = new GitHub();

export const fetchRepos = () => gh.getOrganization('facebook').getRepos();

export const fetchRepoDetails = repo =>
  gh.getRepo('facebook', repo).getDetails();

export const fetchRepoContributors = repo =>
  gh.getRepo('facebook', repo).getContributors();
