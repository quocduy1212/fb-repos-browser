import _ from 'lodash';
import React from 'react';
import PropTypes from 'prop-types';
import Icon from './Icon';

const Repo = ({
  onSelectRepo,
  active,
  name,
  description,
  watchers_count,
  forks_count,
}) => (
  <div
    className={`pointer ph3 pv4 bb b--light-gray ${
      active ? 'bg-light-blue' : ''
    }`}
    onClick={onSelectRepo}
  >
    <div className="f4">{name}</div>
    <div className="f6 mt2">{description}</div>
    <div className="mt2">
      <Icon icon="visibility" text={`${watchers_count}`} />
      <Icon className="ml2" icon="star" text={`${forks_count}`} />
    </div>
  </div>
);

Repo.propTypes = {
  onSelectRepo: PropTypes.func,
  active: PropTypes.bool,
  name: PropTypes.string.isRequired,
  description: PropTypes.string,
  watchers_count: PropTypes.number,
  forks_count: PropTypes.number,
};
Repo.defaultProps = {
  onSelectRepo: _.noop,
  active: false,
  description: '',
  watchers_count: 0,
  forks_count: 0,
};

export default Repo;
