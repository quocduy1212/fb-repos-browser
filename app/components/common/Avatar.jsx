import React from 'react';
import PropTypes from 'prop-types';

const Avatar = ({ className, url, username, size }) => {
  const sizeClass = `w${size} h${size}`;
  return (
    <img
      className={`br2 dib ${sizeClass} ${className}`}
      src={url}
      alt={username}
    />
  );
};

Avatar.propTypes = {
  url: PropTypes.string.isRequired,
  username: PropTypes.string,
  className: PropTypes.string,
  size: PropTypes.oneOf([1, 2, 3, 4, 5]),
};
Avatar.defaultProps = {
  className: '',
  username: '',
  size: 3,
};
export default Avatar;
