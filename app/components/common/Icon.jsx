import React from 'react';
import PropTypes from 'prop-types';

const Icon = ({ className, icon, text }) => (
  <div className={`dib ${className}`}>
    <i className="material-icons f6 v-mid">{icon}</i>
    <span className="f6 ml1">{text}</span>
  </div>
);

Icon.propTypes = {
  icon: PropTypes.string.isRequired,
  text: PropTypes.string,
  className: PropTypes.string,
};
Icon.defaultProps = {
  text: '',
  className: '',
};

export default Icon;
