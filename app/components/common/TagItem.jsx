import React from 'react';
import PropTypes from 'prop-types';

const TagItem = ({ text }) => (
  <span className="dib ph2 pv1 mr2 br1 bg-lightest-blue">{text}</span>
);

TagItem.propTypes = {
  text: PropTypes.string.isRequired,
};

export default TagItem;
