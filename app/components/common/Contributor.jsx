import React from 'react';
import PropTypes from 'prop-types';
import Avatar from './Avatar';

const Contributor = ({ rank, html_url, avatar_url, login, contributions }) => (
  <div className="dib w5">
    <Avatar url={avatar_url} username={login} />
    <div className="dib ml3 pt2 v-top">
      <a href={html_url} className="f4">
        {login}
      </a>
      <div className="mt2">{`${contributions} commits`}</div>
    </div>
    <div className="fr mt2 mr3">{`#${rank}`}</div>
  </div>
);

Contributor.propTypes = {
  rank: PropTypes.number,
  html_url: PropTypes.string,
  avatar_url: PropTypes.string,
  login: PropTypes.string,
  contributions: PropTypes.number,
};
Contributor.defaultProps = {
  rank: 1,
  html_url: '',
  avatar_url: '',
  login: '',
  contributions: 0,
};

export default Contributor;
