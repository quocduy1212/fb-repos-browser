import _ from 'lodash';
import React from 'react';
import PropTypes from 'prop-types';
import withLoader from 'app-hoc/withLoader';
import Repo from 'app-comps/common/Repo';

const RepoList = ({ repos, selectedRepo, onSelectRepo }) => (
  <div className="vh-75 overflow-scroll">
    {repos.map(r => (
      <Repo
        key={r.id}
        {...r}
        active={selectedRepo.name === r.name}
        onSelectRepo={() => onSelectRepo(r)}
      />
    ))}
  </div>
);

RepoList.propTypes = {
  onSelectRepo: PropTypes.func,
  repos: PropTypes.array,
  selectedRepo: PropTypes.object,
};
RepoList.defaultProps = {
  onSelectRepo: _.noop,
  repos: [],
  selectedRepo: {},
};

export default withLoader(RepoList);
