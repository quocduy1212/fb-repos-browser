import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import withLoader from 'app-hoc/withLoader';
import TagItem from 'app-comps/common/TagItem';
import Icon from 'app-comps/common/Icon';

const RepoDetails = ({
  owner,
  name,
  description,
  watchers_count,
  forks_count,
}) => (
  <Fragment>
    <div>
      <i className="material-icons v-mid">storage</i>
      <span className="ml3">{owner.login}</span>
      <span className="ml3">/</span>
      <span className="ml3">{name}</span>
    </div>
    <div className="f6 mt2">{description}</div>
    <div className="f6 mt2">
      <TagItem text="javascript" />
      <TagItem text="ui" />
      <TagItem text="frontend" />
      <TagItem text="declarative" />
      <TagItem text="library" />
    </div>
    <div className="mt2">
      <Icon icon="visibility" text={`${watchers_count}`} />
      <Icon className="ml2" icon="star" text={`${forks_count}`} />
    </div>
  </Fragment>
);

RepoDetails.propTypes = {
  owner: PropTypes.object,
  name: PropTypes.string,
  description: PropTypes.string,
  watchers_count: PropTypes.number,
  forks_count: PropTypes.number,
};
RepoDetails.defaultProps = {
  owner: {},
  name: '',
  description: '',
  watchers_count: 0,
  forks_count: 0,
};

export default withLoader(RepoDetails);
