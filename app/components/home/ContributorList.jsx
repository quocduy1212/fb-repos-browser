import React from 'react';
import PropTypes from 'prop-types';
import withLoader from 'app-hoc/withLoader';
import Contributor from 'app-comps/common/Contributor';

const ContributorList = ({ className, contributors }) => (
  <div className={className}>
    {contributors.map((c, index) => (
      <Contributor key={c.id} {...c} rank={index + 1} />
    ))}
  </div>
);

ContributorList.propTypes = {
  className: PropTypes.string,
  contributors: PropTypes.array,
};
ContributorList.defaultProps = {
  className: '',
  contributors: [],
};

export default withLoader(ContributorList);
