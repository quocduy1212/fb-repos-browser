import _ from 'lodash';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { fetchRepos, selectRepo } from 'app-actions/app';
import * as appSelectors from 'app-selectors/app';
import Avatar from 'app-comps/common/Avatar';
import RepoDetails from './RepoDetails';
import RepoList from './RepoList';
import ContributorList from './ContributorList';

class Home extends Component {
  static propTypes = {
    fetchRepos: PropTypes.func.isRequired,
    onSelectRepo: PropTypes.func.isRequired,
    loading: PropTypes.object,
    selectedRepo: PropTypes.object,
    history: PropTypes.object.isRequired,
    repos: PropTypes.array,
    contributors: PropTypes.array,
    details: PropTypes.object,
    match: PropTypes.object,
    reposError: PropTypes.string,
    detailsError: PropTypes.string,
    contributorsError: PropTypes.string,
  };

  static defaultProps = {
    loading: {},
    selectedRepo: {},
    match: {},
    repos: [],
    contributors: [],
    details: {},
    reposError: '',
    detailsError: '',
    contributorsError: '',
  };

  componentDidMount() {
    this.props.fetchRepos();
    const { match } = this.props;
    if (match.params.repo) {
      this.props.onSelectRepo({ name: match.params.repo });
    }
  }

  shouldComponentUpdate(nextProps) {
    const { match: {}, ...curRest } = this.props;
    const { match: {}, ...nextRest } = nextProps;
    return !_.isEqual(curRest, nextRest);
  }

  onSelectRepo = repo => {
    this.props.history.push(`/${repo.name}`);
    this.props.onSelectRepo(repo);
  };

  render() {
    const {
      selectedRepo,
      loading,
      repos,
      contributors,
      details,
      reposError,
      contributorsError,
      detailsError,
    } = this.props;
    return (
      <div className="cf ph3 pb3">
        <div>
          <Avatar
            className="mv3"
            url="https://avatars3.githubusercontent.com/u/69631?v=4"
            size={4}
          />
        </div>
        <div className="fl w-100 w-third-ns">
          {reposError && <div>{reposError}</div>}
          {!reposError && (
            <RepoList
              isLoading={loading.fetchRepos}
              repos={repos}
              onSelectRepo={this.onSelectRepo}
              selectedRepo={selectedRepo}
            />
          )}
        </div>
        <div className="fl w-100 w-two-thirds-ns pl0 pl4-ns mt4 mt0-ns">
          {detailsError && <div>{detailsError}</div>}
          {!detailsError && (
            <RepoDetails isLoading={loading.fetchRepoDetails} {...details} />
          )}
          {contributorsError && <div>{contributorsError}</div>}
          {!contributorsError && (
            <ContributorList
              className="f6 mt4 vh-50 overflow-scroll"
              isLoading={loading.fetchRepoContributors}
              contributors={contributors}
            />
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  selectedRepo: appSelectors.getSelectedRepo(state),
  loading: appSelectors.getLoading(state),
  repos: appSelectors.getRepos(state),
  contributors: appSelectors.getContributors(state),
  details: appSelectors.getDetails(state),
  reposError: appSelectors.getReposError(state),
  detailsError: appSelectors.getDetailsError(state),
  contributorsError: appSelectors.getContributorsError(state),
});

export default connect(mapStateToProps, {
  fetchRepos,
  onSelectRepo: selectRepo,
})(Home);
